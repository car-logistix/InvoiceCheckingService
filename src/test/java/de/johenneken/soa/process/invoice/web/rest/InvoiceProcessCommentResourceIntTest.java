package de.johenneken.soa.process.invoice.web.rest;

import de.johenneken.soa.process.invoice.InvoiceCheckingServiceApp;

import de.johenneken.soa.process.invoice.domain.InvoiceProcessComment;
import de.johenneken.soa.process.invoice.repository.InvoiceProcessCommentRepository;
import de.johenneken.soa.process.invoice.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;


import static de.johenneken.soa.process.invoice.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the InvoiceProcessCommentResource REST controller.
 *
 * @see InvoiceProcessCommentResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = InvoiceCheckingServiceApp.class)
public class InvoiceProcessCommentResourceIntTest {

    private static final String DEFAULT_TEXT = "AAAAAAAAAA";
    private static final String UPDATED_TEXT = "BBBBBBBBBB";

    private static final String DEFAULT_USER = "AAAAAAAAAA";
    private static final String UPDATED_USER = "BBBBBBBBBB";

    @Autowired
    private InvoiceProcessCommentRepository invoiceProcessCommentRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restInvoiceProcessCommentMockMvc;

    private InvoiceProcessComment invoiceProcessComment;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final InvoiceProcessCommentResource invoiceProcessCommentResource = new InvoiceProcessCommentResource(invoiceProcessCommentRepository);
        this.restInvoiceProcessCommentMockMvc = MockMvcBuilders.standaloneSetup(invoiceProcessCommentResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static InvoiceProcessComment createEntity(EntityManager em) {
        InvoiceProcessComment invoiceProcessComment = new InvoiceProcessComment()
            .text(DEFAULT_TEXT)
            .user(DEFAULT_USER);
        return invoiceProcessComment;
    }

    @Before
    public void initTest() {
        invoiceProcessComment = createEntity(em);
    }

    @Test
    @Transactional
    public void createInvoiceProcessComment() throws Exception {
        int databaseSizeBeforeCreate = invoiceProcessCommentRepository.findAll().size();

        // Create the InvoiceProcessComment
        restInvoiceProcessCommentMockMvc.perform(post("/api/invoice-process-comments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(invoiceProcessComment)))
            .andExpect(status().isCreated());

        // Validate the InvoiceProcessComment in the database
        List<InvoiceProcessComment> invoiceProcessCommentList = invoiceProcessCommentRepository.findAll();
        assertThat(invoiceProcessCommentList).hasSize(databaseSizeBeforeCreate + 1);
        InvoiceProcessComment testInvoiceProcessComment = invoiceProcessCommentList.get(invoiceProcessCommentList.size() - 1);
        assertThat(testInvoiceProcessComment.getText()).isEqualTo(DEFAULT_TEXT);
        assertThat(testInvoiceProcessComment.getUser()).isEqualTo(DEFAULT_USER);
    }

    @Test
    @Transactional
    public void createInvoiceProcessCommentWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = invoiceProcessCommentRepository.findAll().size();

        // Create the InvoiceProcessComment with an existing ID
        invoiceProcessComment.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restInvoiceProcessCommentMockMvc.perform(post("/api/invoice-process-comments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(invoiceProcessComment)))
            .andExpect(status().isBadRequest());

        // Validate the InvoiceProcessComment in the database
        List<InvoiceProcessComment> invoiceProcessCommentList = invoiceProcessCommentRepository.findAll();
        assertThat(invoiceProcessCommentList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkTextIsRequired() throws Exception {
        int databaseSizeBeforeTest = invoiceProcessCommentRepository.findAll().size();
        // set the field null
        invoiceProcessComment.setText(null);

        // Create the InvoiceProcessComment, which fails.

        restInvoiceProcessCommentMockMvc.perform(post("/api/invoice-process-comments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(invoiceProcessComment)))
            .andExpect(status().isBadRequest());

        List<InvoiceProcessComment> invoiceProcessCommentList = invoiceProcessCommentRepository.findAll();
        assertThat(invoiceProcessCommentList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkUserIsRequired() throws Exception {
        int databaseSizeBeforeTest = invoiceProcessCommentRepository.findAll().size();
        // set the field null
        invoiceProcessComment.setUser(null);

        // Create the InvoiceProcessComment, which fails.

        restInvoiceProcessCommentMockMvc.perform(post("/api/invoice-process-comments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(invoiceProcessComment)))
            .andExpect(status().isBadRequest());

        List<InvoiceProcessComment> invoiceProcessCommentList = invoiceProcessCommentRepository.findAll();
        assertThat(invoiceProcessCommentList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllInvoiceProcessComments() throws Exception {
        // Initialize the database
        invoiceProcessCommentRepository.saveAndFlush(invoiceProcessComment);

        // Get all the invoiceProcessCommentList
        restInvoiceProcessCommentMockMvc.perform(get("/api/invoice-process-comments?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(invoiceProcessComment.getId().intValue())))
            .andExpect(jsonPath("$.[*].text").value(hasItem(DEFAULT_TEXT.toString())))
            .andExpect(jsonPath("$.[*].user").value(hasItem(DEFAULT_USER.toString())));
    }
    
    @Test
    @Transactional
    public void getInvoiceProcessComment() throws Exception {
        // Initialize the database
        invoiceProcessCommentRepository.saveAndFlush(invoiceProcessComment);

        // Get the invoiceProcessComment
        restInvoiceProcessCommentMockMvc.perform(get("/api/invoice-process-comments/{id}", invoiceProcessComment.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(invoiceProcessComment.getId().intValue()))
            .andExpect(jsonPath("$.text").value(DEFAULT_TEXT.toString()))
            .andExpect(jsonPath("$.user").value(DEFAULT_USER.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingInvoiceProcessComment() throws Exception {
        // Get the invoiceProcessComment
        restInvoiceProcessCommentMockMvc.perform(get("/api/invoice-process-comments/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateInvoiceProcessComment() throws Exception {
        // Initialize the database
        invoiceProcessCommentRepository.saveAndFlush(invoiceProcessComment);

        int databaseSizeBeforeUpdate = invoiceProcessCommentRepository.findAll().size();

        // Update the invoiceProcessComment
        InvoiceProcessComment updatedInvoiceProcessComment = invoiceProcessCommentRepository.findById(invoiceProcessComment.getId()).get();
        // Disconnect from session so that the updates on updatedInvoiceProcessComment are not directly saved in db
        em.detach(updatedInvoiceProcessComment);
        updatedInvoiceProcessComment
            .text(UPDATED_TEXT)
            .user(UPDATED_USER);

        restInvoiceProcessCommentMockMvc.perform(put("/api/invoice-process-comments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedInvoiceProcessComment)))
            .andExpect(status().isOk());

        // Validate the InvoiceProcessComment in the database
        List<InvoiceProcessComment> invoiceProcessCommentList = invoiceProcessCommentRepository.findAll();
        assertThat(invoiceProcessCommentList).hasSize(databaseSizeBeforeUpdate);
        InvoiceProcessComment testInvoiceProcessComment = invoiceProcessCommentList.get(invoiceProcessCommentList.size() - 1);
        assertThat(testInvoiceProcessComment.getText()).isEqualTo(UPDATED_TEXT);
        assertThat(testInvoiceProcessComment.getUser()).isEqualTo(UPDATED_USER);
    }

    @Test
    @Transactional
    public void updateNonExistingInvoiceProcessComment() throws Exception {
        int databaseSizeBeforeUpdate = invoiceProcessCommentRepository.findAll().size();

        // Create the InvoiceProcessComment

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restInvoiceProcessCommentMockMvc.perform(put("/api/invoice-process-comments")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(invoiceProcessComment)))
            .andExpect(status().isBadRequest());

        // Validate the InvoiceProcessComment in the database
        List<InvoiceProcessComment> invoiceProcessCommentList = invoiceProcessCommentRepository.findAll();
        assertThat(invoiceProcessCommentList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteInvoiceProcessComment() throws Exception {
        // Initialize the database
        invoiceProcessCommentRepository.saveAndFlush(invoiceProcessComment);

        int databaseSizeBeforeDelete = invoiceProcessCommentRepository.findAll().size();

        // Get the invoiceProcessComment
        restInvoiceProcessCommentMockMvc.perform(delete("/api/invoice-process-comments/{id}", invoiceProcessComment.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<InvoiceProcessComment> invoiceProcessCommentList = invoiceProcessCommentRepository.findAll();
        assertThat(invoiceProcessCommentList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(InvoiceProcessComment.class);
        InvoiceProcessComment invoiceProcessComment1 = new InvoiceProcessComment();
        invoiceProcessComment1.setId(1L);
        InvoiceProcessComment invoiceProcessComment2 = new InvoiceProcessComment();
        invoiceProcessComment2.setId(invoiceProcessComment1.getId());
        assertThat(invoiceProcessComment1).isEqualTo(invoiceProcessComment2);
        invoiceProcessComment2.setId(2L);
        assertThat(invoiceProcessComment1).isNotEqualTo(invoiceProcessComment2);
        invoiceProcessComment1.setId(null);
        assertThat(invoiceProcessComment1).isNotEqualTo(invoiceProcessComment2);
    }
}
