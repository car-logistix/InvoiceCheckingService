package de.johenneken.soa.process.invoice.web.rest;

import de.johenneken.soa.process.invoice.InvoiceCheckingServiceApp;

import de.johenneken.soa.process.invoice.domain.InvoiceProcess;
import de.johenneken.soa.process.invoice.repository.InvoiceProcessRepository;
import de.johenneken.soa.process.invoice.service.InvoiceProcessService;
import de.johenneken.soa.process.invoice.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;


import static de.johenneken.soa.process.invoice.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import de.johenneken.soa.process.invoice.domain.enumeration.InvoiceProcessStatus;
/**
 * Test class for the InvoiceProcessResource REST controller.
 *
 * @see InvoiceProcessResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = InvoiceCheckingServiceApp.class)
public class InvoiceProcessResourceIntTest {

    private static final InvoiceProcessStatus DEFAULT_STATUS = InvoiceProcessStatus.INCOMING;
    private static final InvoiceProcessStatus UPDATED_STATUS = InvoiceProcessStatus.FINISHED;

    private static final Long DEFAULT_INVOICE_ID = 1L;
    private static final Long UPDATED_INVOICE_ID = 2L;

    private static final Long DEFAULT_DELIVERY_NOTE_ID = 1L;
    private static final Long UPDATED_DELIVERY_NOTE_ID = 2L;

    private static final String DEFAULT_POSTED_BY = "AAAAAAAAAA";
    private static final String UPDATED_POSTED_BY = "BBBBBBBBBB";

    private static final Instant DEFAULT_POSTED_AT = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_POSTED_AT = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    @Autowired
    private InvoiceProcessRepository invoiceProcessRepository;
    
    @Autowired
    private InvoiceProcessService invoiceProcessService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restInvoiceProcessMockMvc;

    private InvoiceProcess invoiceProcess;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final InvoiceProcessResource invoiceProcessResource = new InvoiceProcessResource(invoiceProcessService);
        this.restInvoiceProcessMockMvc = MockMvcBuilders.standaloneSetup(invoiceProcessResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static InvoiceProcess createEntity(EntityManager em) {
        InvoiceProcess invoiceProcess = new InvoiceProcess()
            .status(DEFAULT_STATUS)
            .invoiceId(DEFAULT_INVOICE_ID)
            .deliveryNoteId(DEFAULT_DELIVERY_NOTE_ID)
            .postedBy(DEFAULT_POSTED_BY)
            .postedAt(DEFAULT_POSTED_AT);
        return invoiceProcess;
    }

    @Before
    public void initTest() {
        invoiceProcess = createEntity(em);
    }

    @Test
    @Transactional
    public void createInvoiceProcess() throws Exception {
        int databaseSizeBeforeCreate = invoiceProcessRepository.findAll().size();

        // Create the InvoiceProcess
        restInvoiceProcessMockMvc.perform(post("/api/invoice-processes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(invoiceProcess)))
            .andExpect(status().isCreated());

        // Validate the InvoiceProcess in the database
        List<InvoiceProcess> invoiceProcessList = invoiceProcessRepository.findAll();
        assertThat(invoiceProcessList).hasSize(databaseSizeBeforeCreate + 1);
        InvoiceProcess testInvoiceProcess = invoiceProcessList.get(invoiceProcessList.size() - 1);
        assertThat(testInvoiceProcess.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testInvoiceProcess.getInvoiceId()).isEqualTo(DEFAULT_INVOICE_ID);
        assertThat(testInvoiceProcess.getDeliveryNoteId()).isEqualTo(DEFAULT_DELIVERY_NOTE_ID);
        assertThat(testInvoiceProcess.getPostedBy()).isEqualTo(DEFAULT_POSTED_BY);
        assertThat(testInvoiceProcess.getPostedAt()).isEqualTo(DEFAULT_POSTED_AT);
    }

    @Test
    @Transactional
    public void createInvoiceProcessWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = invoiceProcessRepository.findAll().size();

        // Create the InvoiceProcess with an existing ID
        invoiceProcess.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restInvoiceProcessMockMvc.perform(post("/api/invoice-processes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(invoiceProcess)))
            .andExpect(status().isBadRequest());

        // Validate the InvoiceProcess in the database
        List<InvoiceProcess> invoiceProcessList = invoiceProcessRepository.findAll();
        assertThat(invoiceProcessList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkStatusIsRequired() throws Exception {
        int databaseSizeBeforeTest = invoiceProcessRepository.findAll().size();
        // set the field null
        invoiceProcess.setStatus(null);

        // Create the InvoiceProcess, which fails.

        restInvoiceProcessMockMvc.perform(post("/api/invoice-processes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(invoiceProcess)))
            .andExpect(status().isBadRequest());

        List<InvoiceProcess> invoiceProcessList = invoiceProcessRepository.findAll();
        assertThat(invoiceProcessList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkInvoiceIdIsRequired() throws Exception {
        int databaseSizeBeforeTest = invoiceProcessRepository.findAll().size();
        // set the field null
        invoiceProcess.setInvoiceId(null);

        // Create the InvoiceProcess, which fails.

        restInvoiceProcessMockMvc.perform(post("/api/invoice-processes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(invoiceProcess)))
            .andExpect(status().isBadRequest());

        List<InvoiceProcess> invoiceProcessList = invoiceProcessRepository.findAll();
        assertThat(invoiceProcessList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllInvoiceProcesses() throws Exception {
        // Initialize the database
        invoiceProcessRepository.saveAndFlush(invoiceProcess);

        // Get all the invoiceProcessList
        restInvoiceProcessMockMvc.perform(get("/api/invoice-processes?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(invoiceProcess.getId().intValue())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].invoiceId").value(hasItem(DEFAULT_INVOICE_ID.intValue())))
            .andExpect(jsonPath("$.[*].deliveryNoteId").value(hasItem(DEFAULT_DELIVERY_NOTE_ID.intValue())))
            .andExpect(jsonPath("$.[*].postedBy").value(hasItem(DEFAULT_POSTED_BY.toString())))
            .andExpect(jsonPath("$.[*].postedAt").value(hasItem(DEFAULT_POSTED_AT.toString())));
    }
    
    @Test
    @Transactional
    public void getInvoiceProcess() throws Exception {
        // Initialize the database
        invoiceProcessRepository.saveAndFlush(invoiceProcess);

        // Get the invoiceProcess
        restInvoiceProcessMockMvc.perform(get("/api/invoice-processes/{id}", invoiceProcess.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(invoiceProcess.getId().intValue()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.invoiceId").value(DEFAULT_INVOICE_ID.intValue()))
            .andExpect(jsonPath("$.deliveryNoteId").value(DEFAULT_DELIVERY_NOTE_ID.intValue()))
            .andExpect(jsonPath("$.postedBy").value(DEFAULT_POSTED_BY.toString()))
            .andExpect(jsonPath("$.postedAt").value(DEFAULT_POSTED_AT.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingInvoiceProcess() throws Exception {
        // Get the invoiceProcess
        restInvoiceProcessMockMvc.perform(get("/api/invoice-processes/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateInvoiceProcess() throws Exception {
        // Initialize the database
        invoiceProcessService.save(invoiceProcess);

        int databaseSizeBeforeUpdate = invoiceProcessRepository.findAll().size();

        // Update the invoiceProcess
        InvoiceProcess updatedInvoiceProcess = invoiceProcessRepository.findById(invoiceProcess.getId()).get();
        // Disconnect from session so that the updates on updatedInvoiceProcess are not directly saved in db
        em.detach(updatedInvoiceProcess);
        updatedInvoiceProcess
            .status(UPDATED_STATUS)
            .invoiceId(UPDATED_INVOICE_ID)
            .deliveryNoteId(UPDATED_DELIVERY_NOTE_ID)
            .postedBy(UPDATED_POSTED_BY)
            .postedAt(UPDATED_POSTED_AT);

        restInvoiceProcessMockMvc.perform(put("/api/invoice-processes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedInvoiceProcess)))
            .andExpect(status().isOk());

        // Validate the InvoiceProcess in the database
        List<InvoiceProcess> invoiceProcessList = invoiceProcessRepository.findAll();
        assertThat(invoiceProcessList).hasSize(databaseSizeBeforeUpdate);
        InvoiceProcess testInvoiceProcess = invoiceProcessList.get(invoiceProcessList.size() - 1);
        assertThat(testInvoiceProcess.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testInvoiceProcess.getInvoiceId()).isEqualTo(UPDATED_INVOICE_ID);
        assertThat(testInvoiceProcess.getDeliveryNoteId()).isEqualTo(UPDATED_DELIVERY_NOTE_ID);
        assertThat(testInvoiceProcess.getPostedBy()).isEqualTo(UPDATED_POSTED_BY);
        assertThat(testInvoiceProcess.getPostedAt()).isEqualTo(UPDATED_POSTED_AT);
    }

    @Test
    @Transactional
    public void updateNonExistingInvoiceProcess() throws Exception {
        int databaseSizeBeforeUpdate = invoiceProcessRepository.findAll().size();

        // Create the InvoiceProcess

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restInvoiceProcessMockMvc.perform(put("/api/invoice-processes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(invoiceProcess)))
            .andExpect(status().isBadRequest());

        // Validate the InvoiceProcess in the database
        List<InvoiceProcess> invoiceProcessList = invoiceProcessRepository.findAll();
        assertThat(invoiceProcessList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteInvoiceProcess() throws Exception {
        // Initialize the database
        invoiceProcessService.save(invoiceProcess);

        int databaseSizeBeforeDelete = invoiceProcessRepository.findAll().size();

        // Get the invoiceProcess
        restInvoiceProcessMockMvc.perform(delete("/api/invoice-processes/{id}", invoiceProcess.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<InvoiceProcess> invoiceProcessList = invoiceProcessRepository.findAll();
        assertThat(invoiceProcessList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(InvoiceProcess.class);
        InvoiceProcess invoiceProcess1 = new InvoiceProcess();
        invoiceProcess1.setId(1L);
        InvoiceProcess invoiceProcess2 = new InvoiceProcess();
        invoiceProcess2.setId(invoiceProcess1.getId());
        assertThat(invoiceProcess1).isEqualTo(invoiceProcess2);
        invoiceProcess2.setId(2L);
        assertThat(invoiceProcess1).isNotEqualTo(invoiceProcess2);
        invoiceProcess1.setId(null);
        assertThat(invoiceProcess1).isNotEqualTo(invoiceProcess2);
    }
}
