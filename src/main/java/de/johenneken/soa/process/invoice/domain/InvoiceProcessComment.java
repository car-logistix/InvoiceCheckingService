package de.johenneken.soa.process.invoice.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A InvoiceProcessComment.
 */
@Entity
@Table(name = "invoice_process_comment")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class InvoiceProcessComment implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "text", nullable = false)
    private String text;

    @NotNull
    @Column(name = "jhi_user", nullable = false)
    private String user;

    @ManyToOne
    @JsonIgnoreProperties("comments")
    private InvoiceProcess invoiceProcess;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public InvoiceProcessComment text(String text) {
        this.text = text;
        return this;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getUser() {
        return user;
    }

    public InvoiceProcessComment user(String user) {
        this.user = user;
        return this;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public InvoiceProcess getInvoiceProcess() {
        return invoiceProcess;
    }

    public InvoiceProcessComment invoiceProcess(InvoiceProcess invoiceProcess) {
        this.invoiceProcess = invoiceProcess;
        return this;
    }

    public void setInvoiceProcess(InvoiceProcess invoiceProcess) {
        this.invoiceProcess = invoiceProcess;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        InvoiceProcessComment invoiceProcessComment = (InvoiceProcessComment) o;
        if (invoiceProcessComment.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), invoiceProcessComment.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "InvoiceProcessComment{" +
            "id=" + getId() +
            ", text='" + getText() + "'" +
            ", user='" + getUser() + "'" +
            "}";
    }
}
