package de.johenneken.soa.process.invoice.service.impl;

import de.johenneken.soa.process.invoice.client.BREClient;
import de.johenneken.soa.process.invoice.client.InvoiceClient;
import de.johenneken.soa.process.invoice.domain.InvoiceDTO;
import de.johenneken.soa.process.invoice.domain.InvoiceEscalationDTO;
import de.johenneken.soa.process.invoice.domain.InvoiceProcess;
import de.johenneken.soa.process.invoice.service.EscalationNotificationService;
import io.github.jhipster.config.JHipsterProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

import javax.mail.internet.MimeMessage;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Locale;

@Service
public class EscalationNotificationServiceImpl implements EscalationNotificationService {

    private final Logger log = LoggerFactory.getLogger(EscalationNotificationServiceImpl.class);

    private static final String INVOICE_PROCESS = "invoiceProcess";

    private static final String BASE_URL = "baseUrl";

    private final JHipsterProperties jHipsterProperties;

    private final JavaMailSender javaMailSender;

    private final SpringTemplateEngine templateEngine;
    private final BREClient breClient;
    private final InvoiceClient invoiceClient;

    public EscalationNotificationServiceImpl(JHipsterProperties jHipsterProperties, JavaMailSender javaMailSender,
                                             SpringTemplateEngine templateEngine, BREClient breClient, InvoiceClient invoiceClient) {
        this.jHipsterProperties = jHipsterProperties;
        this.javaMailSender = javaMailSender;
        this.templateEngine = templateEngine;
        this.breClient = breClient;
        this.invoiceClient = invoiceClient;
    }

    @Override
    @Async
    public void sendEscalationMail(InvoiceProcess invoiceProcess) {
        InvoiceEscalationDTO invoiceEscalationDTO = new InvoiceEscalationDTO();
        InvoiceDTO invoice = invoiceClient.getInvoice(invoiceProcess.getInvoiceId());
        invoiceEscalationDTO.setTotal(invoice.getTotal());
        invoiceEscalationDTO.setSupplier(invoice.getIssuerFirm());
        if(invoice.getDueDate()!=null) {
            invoiceEscalationDTO.setOverdue(invoice.getDueDate().isBefore(LocalDate.now()));
        }
        List<String> mailAdresses = breClient.evaluateInvoicesForContacts(invoiceEscalationDTO);
        mailAdresses.forEach(mail -> sendEmailFromTemplate("mail/escalationEmail",invoiceProcess,mail));
    }

    @Async
    public void sendEmailFromTemplate(String templateName, InvoiceProcess invoiceProcess,String to) {
        Locale locale = Locale.GERMAN;
        Context context = new Context(locale);
        context.setVariable(INVOICE_PROCESS,invoiceProcess);
        context.setVariable(BASE_URL, jHipsterProperties.getMail().getBaseUrl());
        String content = templateEngine.process(templateName, context);
        String subject = "Rechnung wurde eskaliert";
        sendEmail(to, subject, content, false, true);

    }

    @Async
    public void sendEmail(String to, String subject, String content, boolean isMultipart, boolean isHtml) {
        log.debug("Send email[multipart '{}' and html '{}'] to '{}' with subject '{}' and content={}",
            isMultipart, isHtml, to, subject, content);

        // Prepare message using a Spring helper
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        try {
            MimeMessageHelper message = new MimeMessageHelper(mimeMessage, isMultipart, StandardCharsets.UTF_8.name());
            message.setTo(to);
            message.setFrom(jHipsterProperties.getMail().getFrom());
            message.setSubject(subject);
            message.setText(content, isHtml);
            javaMailSender.send(mimeMessage);
            log.debug("Sent email to User '{}'", to);
        } catch (Exception e) {
            if (log.isDebugEnabled()) {
                log.warn("Email could not be sent to user '{}'", to, e);
            } else {
                log.warn("Email could not be sent to user '{}': {}", to, e.getMessage());
            }
        }
    }
}
