package de.johenneken.soa.process.invoice.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;

/**
 * A InvoiceComment.
 */
public class InvoiceComment implements Serializable {

    private static final long serialVersionUID = 1L;


    private Long id;

    @NotNull
    private String text;

    @NotNull
    private String user;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public InvoiceComment text(String text) {
        this.text = text;
        return this;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getUser() {
        return user;
    }

    public InvoiceComment user(String user) {
        this.user = user;
        return this;
    }

    public void setUser(String user) {
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        InvoiceComment invoiceComment = (InvoiceComment) o;
        if (invoiceComment.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), invoiceComment.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "InvoiceComment{" +
            "id=" + getId() +
            ", text='" + getText() + "'" +
            ", user='" + getUser() + "'" +
            "}";
    }
}
