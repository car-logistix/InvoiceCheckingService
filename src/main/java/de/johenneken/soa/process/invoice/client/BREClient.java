package de.johenneken.soa.process.invoice.client;

import de.johenneken.soa.process.invoice.domain.InvoiceEscalationDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@FeignClient("BRE")
public interface BREClient {

    @RequestMapping(method = RequestMethod.POST, path = "api/rules/escalation")
    List<String> evaluateInvoicesForContacts(InvoiceEscalationDTO invoiceEscalationDTO);
}
