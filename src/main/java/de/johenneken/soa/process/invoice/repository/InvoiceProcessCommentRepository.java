package de.johenneken.soa.process.invoice.repository;

import de.johenneken.soa.process.invoice.domain.InvoiceProcessComment;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the InvoiceProcessComment entity.
 */
@SuppressWarnings("unused")
@Repository
public interface InvoiceProcessCommentRepository extends JpaRepository<InvoiceProcessComment, Long> {

}
