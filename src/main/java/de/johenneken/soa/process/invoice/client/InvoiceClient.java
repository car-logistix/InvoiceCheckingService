package de.johenneken.soa.process.invoice.client;

import de.johenneken.soa.process.invoice.domain.InvoiceDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@FeignClient("InvoiceService")
public interface InvoiceClient {

    @RequestMapping(method = RequestMethod.GET,value = "/api/invoices")
    List<InvoiceDTO> getInvoices();

    @RequestMapping(method = RequestMethod.POST,value = "api/invoices/{id}/finish")
    void finishInvoice(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.POST,value = "api/invoices/{id}/accept")
    void acceptInvoice(@PathVariable("id") Long id);

    @RequestMapping(method = RequestMethod.GET,value = "api/invoices/{id}")
    InvoiceDTO getInvoice(@PathVariable("id") Long invoiceId);
}
