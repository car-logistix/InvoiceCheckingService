/**
 * View Models used by Spring MVC REST controllers.
 */
package de.johenneken.soa.process.invoice.web.rest.vm;
