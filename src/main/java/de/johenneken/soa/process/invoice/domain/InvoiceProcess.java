package de.johenneken.soa.process.invoice.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import de.johenneken.soa.process.invoice.domain.enumeration.InvoiceProcessStatus;

/**
 * A InvoiceProcess.
 */
@Entity
@Table(name = "invoice_process")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class InvoiceProcess implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "status", nullable = false)
    private InvoiceProcessStatus status;

    @NotNull
    @Column(name = "invoice_id", nullable = false)
    private Long invoiceId;

    @Column(name = "delivery_note_id")
    private Long deliveryNoteId;

    @Column(name = "posted_by")
    private String postedBy;

    @Column(name = "posted_at")
    private Instant postedAt;

    @OneToMany(mappedBy = "invoiceProcess", fetch = FetchType.EAGER)
    @JsonIgnoreProperties("invoiceProcess")
    private Set<InvoiceProcessComment> comments = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public InvoiceProcessStatus getStatus() {
        return status;
    }

    public InvoiceProcess status(InvoiceProcessStatus status) {
        this.status = status;
        return this;
    }

    public void setStatus(InvoiceProcessStatus status) {
        this.status = status;
    }

    public Long getInvoiceId() {
        return invoiceId;
    }

    public InvoiceProcess invoiceId(Long invoiceId) {
        this.invoiceId = invoiceId;
        return this;
    }

    public void setInvoiceId(Long invoiceId) {
        this.invoiceId = invoiceId;
    }

    public Long getDeliveryNoteId() {
        return deliveryNoteId;
    }

    public InvoiceProcess deliveryNoteId(Long deliveryNoteId) {
        this.deliveryNoteId = deliveryNoteId;
        return this;
    }

    public void setDeliveryNoteId(Long deliveryNoteId) {
        this.deliveryNoteId = deliveryNoteId;
    }

    public String getPostedBy() {
        return postedBy;
    }

    public InvoiceProcess postedBy(String postedBy) {
        this.postedBy = postedBy;
        return this;
    }

    public void setPostedBy(String postedBy) {
        this.postedBy = postedBy;
    }

    public Instant getPostedAt() {
        return postedAt;
    }

    public InvoiceProcess postedAt(Instant postedAt) {
        this.postedAt = postedAt;
        return this;
    }

    public void setPostedAt(Instant postedAt) {
        this.postedAt = postedAt;
    }

    public Set<InvoiceProcessComment> getComments() {
        return comments;
    }

    public InvoiceProcess comments(Set<InvoiceProcessComment> invoiceProcessComments) {
        this.comments = invoiceProcessComments;
        return this;
    }

    public InvoiceProcess addComments(InvoiceProcessComment invoiceProcessComment) {
        this.comments.add(invoiceProcessComment);
        invoiceProcessComment.setInvoiceProcess(this);
        return this;
    }

    public InvoiceProcess removeComments(InvoiceProcessComment invoiceProcessComment) {
        this.comments.remove(invoiceProcessComment);
        invoiceProcessComment.setInvoiceProcess(null);
        return this;
    }

    public void setComments(Set<InvoiceProcessComment> invoiceProcessComments) {
        this.comments = invoiceProcessComments;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        InvoiceProcess invoiceProcess = (InvoiceProcess) o;
        if (invoiceProcess.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), invoiceProcess.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "InvoiceProcess{" +
            "id=" + getId() +
            ", status='" + getStatus() + "'" +
            ", invoiceId=" + getInvoiceId() +
            ", deliveryNoteId=" + getDeliveryNoteId() +
            ", postedBy='" + getPostedBy() + "'" +
            ", postedAt='" + getPostedAt() + "'" +
            "}";
    }
}
