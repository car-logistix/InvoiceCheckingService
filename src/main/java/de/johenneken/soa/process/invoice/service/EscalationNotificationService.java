package de.johenneken.soa.process.invoice.service;

import de.johenneken.soa.process.invoice.domain.InvoiceProcess;
import org.springframework.scheduling.annotation.Async;

public interface EscalationNotificationService {

    @Async
    void sendEscalationMail(InvoiceProcess invoiceProcess);
}
