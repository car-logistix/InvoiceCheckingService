package de.johenneken.soa.process.invoice.web.rest;

import com.codahale.metrics.annotation.Timed;
import de.johenneken.soa.process.invoice.domain.InvoiceProcess;
import de.johenneken.soa.process.invoice.service.InvoiceProcessService;
import de.johenneken.soa.process.invoice.web.rest.errors.BadRequestAlertException;
import de.johenneken.soa.process.invoice.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing InvoiceProcess.
 */
@RestController
@RequestMapping("/api")
public class InvoiceProcessResource {

    private final Logger log = LoggerFactory.getLogger(InvoiceProcessResource.class);

    private static final String ENTITY_NAME = "invoiceCheckingServiceInvoiceProcess";

    private final InvoiceProcessService invoiceProcessService;

    public InvoiceProcessResource(InvoiceProcessService invoiceProcessService) {
        this.invoiceProcessService = invoiceProcessService;
    }

    /**
     * POST  /invoice-processes : Create a new invoiceProcess.
     *
     * @param invoiceProcess the invoiceProcess to create
     * @return the ResponseEntity with status 201 (Created) and with body the new invoiceProcess, or with status 400 (Bad Request) if the invoiceProcess has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/invoice-processes")
    @Timed
    public ResponseEntity<InvoiceProcess> createInvoiceProcess(@Valid @RequestBody InvoiceProcess invoiceProcess) throws URISyntaxException {
        log.debug("REST request to save InvoiceProcess : {}", invoiceProcess);
        if (invoiceProcess.getId() != null) {
            throw new BadRequestAlertException("A new invoiceProcess cannot already have an ID", ENTITY_NAME, "idexists");
        }
        InvoiceProcess result = invoiceProcessService.save(invoiceProcess);
        return ResponseEntity.created(new URI("/api/invoice-processes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /invoice-processes : Updates an existing invoiceProcess.
     *
     * @param invoiceProcess the invoiceProcess to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated invoiceProcess,
     * or with status 400 (Bad Request) if the invoiceProcess is not valid,
     * or with status 500 (Internal Server Error) if the invoiceProcess couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/invoice-processes")
    @Timed
    public ResponseEntity<InvoiceProcess> updateInvoiceProcess(@Valid @RequestBody InvoiceProcess invoiceProcess) throws URISyntaxException {
        log.debug("REST request to update InvoiceProcess : {}", invoiceProcess);
        if (invoiceProcess.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        InvoiceProcess result = invoiceProcessService.save(invoiceProcess);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, invoiceProcess.getId().toString()))
            .body(result);
    }

    /**
     * GET  /invoice-processes : get all the invoiceProcesses.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of invoiceProcesses in body
     */
    @GetMapping("/invoice-processes")
    @Timed
    public List<InvoiceProcess> getAllInvoiceProcesses() {
        log.debug("REST request to get all InvoiceProcesses");
        return invoiceProcessService.findAll();
    }

    /**
     * GET  /invoice-processes/:id : get the "id" invoiceProcess.
     *
     * @param id the id of the invoiceProcess to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the invoiceProcess, or with status 404 (Not Found)
     */
    @GetMapping("/invoice-processes/{id}")
    @Timed
    public ResponseEntity<InvoiceProcess> getInvoiceProcess(@PathVariable Long id) {
        log.debug("REST request to get InvoiceProcess : {}", id);
        Optional<InvoiceProcess> invoiceProcess = invoiceProcessService.findOne(id);
        return ResponseUtil.wrapOrNotFound(invoiceProcess);
    }

    /**
     * DELETE  /invoice-processes/:id : delete the "id" invoiceProcess.
     *
     * @param id the id of the invoiceProcess to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/invoice-processes/{id}")
    @Timed
    public ResponseEntity<Void> deleteInvoiceProcess(@PathVariable Long id) {
        log.debug("REST request to delete InvoiceProcess : {}", id);
        invoiceProcessService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    @PostMapping("/invoice-processes/{id}/escalate")
    @Timed
    public ResponseEntity<Void> escalateInvoiceProcess(@PathVariable Long id) {
        log.debug("REST request to escalate InvoiceProcess: {}",id);
        invoiceProcessService.escalate(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME,id.toString())).build();
    }

    @PostMapping("/invoice-processes/{id}/finish")
    @Timed
    public ResponseEntity<Void> finishInvoiceProcess(@PathVariable Long id) {
        log.debug("REST request to finish InvoiceProcess: {}",id);
        invoiceProcessService.finish(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME,id.toString())).build();
    }
}
