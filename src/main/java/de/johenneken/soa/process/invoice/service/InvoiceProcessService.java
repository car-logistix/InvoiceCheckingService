package de.johenneken.soa.process.invoice.service;

import de.johenneken.soa.process.invoice.domain.InvoiceProcess;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing InvoiceProcess.
 */
public interface InvoiceProcessService {

    /**
     * Save a invoiceProcess.
     *
     * @param invoiceProcess the entity to save
     * @return the persisted entity
     */
    InvoiceProcess save(InvoiceProcess invoiceProcess);

    /**
     * Get all the invoiceProcesses.
     *
     * @return the list of entities
     */
    List<InvoiceProcess> findAll();


    /**
     * Get the "id" invoiceProcess.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<InvoiceProcess> findOne(Long id);

    /**
     * Delete the "id" invoiceProcess.
     *
     * @param id the id of the entity
     */
    void delete(Long id);

    void escalate(Long id);

    void finish(Long id);
}
