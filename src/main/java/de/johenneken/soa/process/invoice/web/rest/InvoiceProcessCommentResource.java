package de.johenneken.soa.process.invoice.web.rest;

import com.codahale.metrics.annotation.Timed;
import de.johenneken.soa.process.invoice.domain.InvoiceProcessComment;
import de.johenneken.soa.process.invoice.repository.InvoiceProcessCommentRepository;
import de.johenneken.soa.process.invoice.web.rest.errors.BadRequestAlertException;
import de.johenneken.soa.process.invoice.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing InvoiceProcessComment.
 */
@RestController
@RequestMapping("/api")
public class InvoiceProcessCommentResource {

    private final Logger log = LoggerFactory.getLogger(InvoiceProcessCommentResource.class);

    private static final String ENTITY_NAME = "invoiceCheckingServiceInvoiceProcessComment";

    private final InvoiceProcessCommentRepository invoiceProcessCommentRepository;

    public InvoiceProcessCommentResource(InvoiceProcessCommentRepository invoiceProcessCommentRepository) {
        this.invoiceProcessCommentRepository = invoiceProcessCommentRepository;
    }

    /**
     * POST  /invoice-process-comments : Create a new invoiceProcessComment.
     *
     * @param invoiceProcessComment the invoiceProcessComment to create
     * @return the ResponseEntity with status 201 (Created) and with body the new invoiceProcessComment, or with status 400 (Bad Request) if the invoiceProcessComment has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/invoice-process-comments")
    @Timed
    public ResponseEntity<InvoiceProcessComment> createInvoiceProcessComment(@Valid @RequestBody InvoiceProcessComment invoiceProcessComment) throws URISyntaxException {
        log.debug("REST request to save InvoiceProcessComment : {}", invoiceProcessComment);
        if (invoiceProcessComment.getId() != null) {
            throw new BadRequestAlertException("A new invoiceProcessComment cannot already have an ID", ENTITY_NAME, "idexists");
        }
        InvoiceProcessComment result = invoiceProcessCommentRepository.save(invoiceProcessComment);
        return ResponseEntity.created(new URI("/api/invoice-process-comments/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /invoice-process-comments : Updates an existing invoiceProcessComment.
     *
     * @param invoiceProcessComment the invoiceProcessComment to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated invoiceProcessComment,
     * or with status 400 (Bad Request) if the invoiceProcessComment is not valid,
     * or with status 500 (Internal Server Error) if the invoiceProcessComment couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/invoice-process-comments")
    @Timed
    public ResponseEntity<InvoiceProcessComment> updateInvoiceProcessComment(@Valid @RequestBody InvoiceProcessComment invoiceProcessComment) throws URISyntaxException {
        log.debug("REST request to update InvoiceProcessComment : {}", invoiceProcessComment);
        if (invoiceProcessComment.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        InvoiceProcessComment result = invoiceProcessCommentRepository.save(invoiceProcessComment);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, invoiceProcessComment.getId().toString()))
            .body(result);
    }

    /**
     * GET  /invoice-process-comments : get all the invoiceProcessComments.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of invoiceProcessComments in body
     */
    @GetMapping("/invoice-process-comments")
    @Timed
    public List<InvoiceProcessComment> getAllInvoiceProcessComments() {
        log.debug("REST request to get all InvoiceProcessComments");
        return invoiceProcessCommentRepository.findAll();
    }

    /**
     * GET  /invoice-process-comments/:id : get the "id" invoiceProcessComment.
     *
     * @param id the id of the invoiceProcessComment to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the invoiceProcessComment, or with status 404 (Not Found)
     */
    @GetMapping("/invoice-process-comments/{id}")
    @Timed
    public ResponseEntity<InvoiceProcessComment> getInvoiceProcessComment(@PathVariable Long id) {
        log.debug("REST request to get InvoiceProcessComment : {}", id);
        Optional<InvoiceProcessComment> invoiceProcessComment = invoiceProcessCommentRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(invoiceProcessComment);
    }

    /**
     * DELETE  /invoice-process-comments/:id : delete the "id" invoiceProcessComment.
     *
     * @param id the id of the invoiceProcessComment to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/invoice-process-comments/{id}")
    @Timed
    public ResponseEntity<Void> deleteInvoiceProcessComment(@PathVariable Long id) {
        log.debug("REST request to delete InvoiceProcessComment : {}", id);

        invoiceProcessCommentRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
