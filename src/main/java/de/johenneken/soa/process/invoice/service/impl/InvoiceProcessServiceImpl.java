package de.johenneken.soa.process.invoice.service.impl;

import de.johenneken.soa.process.invoice.client.InvoiceClient;
import de.johenneken.soa.process.invoice.domain.InvoiceDTO;
import de.johenneken.soa.process.invoice.domain.enumeration.InvoiceProcessStatus;
import de.johenneken.soa.process.invoice.domain.enumeration.InvoiceStatus;
import de.johenneken.soa.process.invoice.repository.InvoiceProcessCommentRepository;
import de.johenneken.soa.process.invoice.service.EscalationNotificationService;
import de.johenneken.soa.process.invoice.service.InvoiceProcessService;
import de.johenneken.soa.process.invoice.domain.InvoiceProcess;
import de.johenneken.soa.process.invoice.repository.InvoiceProcessRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing InvoiceProcess.
 */
@Service
@Transactional
public class InvoiceProcessServiceImpl implements InvoiceProcessService {

    private final Logger log = LoggerFactory.getLogger(InvoiceProcessServiceImpl.class);

    private final InvoiceProcessRepository invoiceProcessRepository;
    private final InvoiceClient invoiceClient;
    private final EscalationNotificationService escalationNotificationService;
    private final InvoiceProcessCommentRepository invoiceProcessCommentRepository;


    public InvoiceProcessServiceImpl(InvoiceProcessRepository invoiceProcessRepository, InvoiceClient invoiceClient, EscalationNotificationService escalationNotificationService, InvoiceProcessCommentRepository invoiceProcessCommentRepository) {
        this.invoiceProcessRepository = invoiceProcessRepository;
        this.invoiceClient = invoiceClient;
        this.escalationNotificationService = escalationNotificationService;
        this.invoiceProcessCommentRepository = invoiceProcessCommentRepository;
    }

    /**
     * Save a invoiceProcess.
     *
     * @param invoiceProcess the entity to save
     * @return the persisted entity
     */
    @Override
    public InvoiceProcess save(InvoiceProcess invoiceProcess) {
        log.debug("Request to save InvoiceProcess : {}", invoiceProcess);
        invoiceProcess.getComments().forEach(invoiceProcessComment -> invoiceProcessComment.setInvoiceProcess(invoiceProcess));
        invoiceProcessCommentRepository.saveAll(invoiceProcess.getComments());
        return invoiceProcessRepository.save(invoiceProcess);
    }

    /**
     * Get all the invoiceProcesses.
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<InvoiceProcess> findAll() {
        log.debug("Request to get all InvoiceProcesses");
        return invoiceProcessRepository.findAll();
    }


    /**
     * Get one invoiceProcess by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<InvoiceProcess> findOne(Long id) {
        log.debug("Request to get InvoiceProcess : {}", id);
        return invoiceProcessRepository.findById(id);
    }

    /**
     * Delete the invoiceProcess by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete InvoiceProcess : {}", id);
        invoiceProcessRepository.deleteById(id);
    }

    @Override
    public void escalate(Long id) {
        InvoiceProcess invoiceProcess = invoiceProcessRepository.findById(id).get();
        invoiceProcess.setStatus(InvoiceProcessStatus.ESCALATED);
        invoiceProcessRepository.save(invoiceProcess);
        escalationNotificationService.sendEscalationMail(invoiceProcess);
    }

    @Override
    public void finish(Long id) {
        InvoiceProcess invoiceProcess = invoiceProcessRepository.findById(id).get();
        invoiceProcess.setStatus(InvoiceProcessStatus.FINISHED);
        invoiceProcessRepository.save(invoiceProcess);
    }

    @Scheduled(fixedDelay = 15000,initialDelay = 60000)
    public void checkForNewInvoices() {
        log.debug("Check for new Invoices");
        List<InvoiceDTO> invoices = invoiceClient.getInvoices();
        for (InvoiceDTO invoiceDTO : invoices) {
            if (invoiceDTO.getStatus() == InvoiceStatus.SENT) {
                InvoiceProcess invoiceProcess = new InvoiceProcess()
                    .invoiceId(invoiceDTO.getId())
                    .status(InvoiceProcessStatus.INCOMING);
                invoiceClient.acceptInvoice(invoiceDTO.getId());
                invoiceProcessRepository.save(invoiceProcess);
                log.info("Created InvoiceProcess for Invoice with id:{} at timestamp:{}", invoiceDTO.getId(), Instant.now());
            }
        }

    }

}
