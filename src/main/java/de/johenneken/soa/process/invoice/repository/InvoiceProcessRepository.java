package de.johenneken.soa.process.invoice.repository;

import de.johenneken.soa.process.invoice.domain.InvoiceProcess;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the InvoiceProcess entity.
 */
@SuppressWarnings("unused")
@Repository
public interface InvoiceProcessRepository extends JpaRepository<InvoiceProcess, Long> {

}
