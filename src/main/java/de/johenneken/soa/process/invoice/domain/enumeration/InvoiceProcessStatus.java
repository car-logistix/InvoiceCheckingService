package de.johenneken.soa.process.invoice.domain.enumeration;

/**
 * The InvoiceProcessStatus enumeration.
 */
public enum InvoiceProcessStatus {
    INCOMING, FINISHED, ESCALATED
}
