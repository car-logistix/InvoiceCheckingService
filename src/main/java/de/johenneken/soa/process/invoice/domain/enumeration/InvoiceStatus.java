package de.johenneken.soa.process.invoice.domain.enumeration;

/**
 * The InvoiceStatus enumeration.
 */
public enum InvoiceStatus {
    NEW, SENT, ACCEPTED, FINISHED
}
